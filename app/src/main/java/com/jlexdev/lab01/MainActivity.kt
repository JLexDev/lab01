package com.jlexdev.lab01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Button event
        btnProcess.setOnClickListener {

            // Get Age
            val age = etAge.text.toString()

            // Validation
            if (age.isEmpty()) {
                Toast.makeText(this, "Ingrese edad", Toast.LENGTH_SHORT).show()
                tvResult.text = ""
                return@setOnClickListener
            }

            // Get Age value
            val ageValue = age.toInt()

            /* Show result
            if  (ageValue < 18) {
                tvResult.text = "Su edad es $ageValue, usted es menor de edad."
            } else {
                tvResult.text = "Su edad es $ageValue, usted es mayor de edad."
            }*/

            // Show result
            val result = if (ageValue < 18) "menor" else "mayor"
            tvResult.text = "Su edad es $ageValue,\n usted es $result de edad"
        }

    }


}